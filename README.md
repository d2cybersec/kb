# Knowledge Base

Веб-сайт 

<https://i2z1.quarto.pub/d2csec/>

<https://d2cybersec.gitlab.io/kb/>

Проект аккумулирующий Базу знаний проекта Data-Driven CyberSec Lab <https://d2cybersec.gitlab.io/>

## Как принять участие

Эта База знаний реализована с помощью [Quarto](https://quarto.org/) -- системы публикации построенной на конвертере [Pandoc](https://pandoc.org/)

Если Вы хотите внести изменения, добавить материал:

1. Сделате форк проекта под своей учетной записью -- <https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html>
2. Внесите изменения (рекомендуем Rstudio IDE -- <https://quarto.org/docs/tools/rstudio.html>)
3. Оформите Merge Request -- <https://docs.gitlab.com/ee/user/project/merge_requests/>

## License

MIT License


